import static com.sun.java.accessibility.util.TopLevelWindowMulticaster.remove;

//二叉搜索树
public class BinarySearchTree {
    static class TreeNode{
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int val){
            this.val = val;
        }
    }

    public TreeNode root;

    public TreeNode search(int key){
        TreeNode cur = root;

        while (cur != null){
            if (cur.val > key){
                cur = cur.left;
            }else if (cur.val < key){
                cur = cur.right;
            }else{
                return cur;
            }
        }

        return null;
    }
}
