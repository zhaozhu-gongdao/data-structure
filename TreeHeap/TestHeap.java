import java.util.*;

class Student implements Comparable<Student>{
    public String name;
    private Integer age;

    @Override
    public int compareTo(Student o) {
        return o.age - this.age;
    }

    public Student(String name, Integer age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }

        if (obj == null || getClass() != obj.getClass()){
            return false;
        }

        Student student = (Student) obj;
        return age == student.age && Objects.equals(name, student.name);
    }
}

//名称的比较器
class NameComparator implements Comparator<Student>{
    @Override
    public int compare(Student o1, Student o2) {
        return o2.name.compareTo(o1.name);
    }
}

class ComparByBig implements Comparator<Integer>{
    @Override
    public int compare(Integer o1, Integer o2) {
        return o2 - o1;
    }
}
public class TestHeap {
    public static void main(String[] args) {
//        List<Integer> list = new ArrayList<>();
//        list.add(18);
//        list.add(10);
//        Queue<Integer> queue = new PriorityQueue<>(list);
//        queue.offer(2);
//        queue.offer(34);
        Student oldSecond = new Student("zhangsan", 10);
        Student oldThird = new Student("lisi", 15);
        Student oldFourth = new Student("wangwu", 30);
        Student oldFirst = new Student("zhaoliu", 8);
        Student oldFifth = new Student("masheng", 42);
        NameComparator nameComparator = new NameComparator();
        //Student[] students = new Student[]{oldFifth, oldFourth, oldThird, oldSecond, oldFirst};

        Queue<Student> queue = new PriorityQueue<>(nameComparator);
        queue.offer(oldFifth);
        queue.offer(oldFirst);
        queue.offer(oldFourth);
        queue.offer(oldSecond);
        queue.offer(oldThird);
//        NameComparator nameComparator = new NameComparator();
//        System.out.println(nameComparator.compare(s1, s2));

        int[] ret = smallestK(new int[]{19, 6, 32, 2, 0}, 3);


    }

    //寻找前k个最小的数
    public static int[] smallestK(int[] arr, int k){
        Queue<Integer> maxHeap = new PriorityQueue<>(new ComparByBig());

        for (int i = 0; i < k; i++){
            maxHeap.offer(arr[i]);
        }

        for (int i = k; i < arr.length; i++){
            int top = maxHeap.peek();

            if (top > arr[i]){
                maxHeap.poll();
                maxHeap.offer(arr[i]);
            }
        }

        int[] ret = new int[k];
        for (int i = 0; i < k; i++){
            ret[i] = maxHeap.poll();
        }

        return ret;
    }

}

