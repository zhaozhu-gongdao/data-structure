import java.util.Objects;

class Person{
    private String name;

    public Person(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
public class TestHashCode {
    public static void main(String[] args) {
        Person person1 = new Person("zhangsan");
        Person person2 = new Person("zhangsan");
        System.out.println(person1.hashCode());
        System.out.println(person2.hashCode());
    }
}
